#!/usr/bin/env ruby
require 'optparse'
require 'csv'

BOOKS_FILE = File.expand_path("../books.csv", __FILE__)

class Book
  attr_accessor :authors
  attr_reader :title
  attr_reader :language_id
  attr_reader :publisher_id

  def initialize(title, language_id, publisher_id, year, authors)
    @title = title
    @language_id = language_id
    @publisher_id = publisher_id
    @year = year
    @authors = authors
  end

  def print
    unless @authors.size == 0
      s = @authors[0].name.to_s
      i = 1
      while i < @authors.size
        s += ", " + @authors[i].name.to_s
        i += 1
      end
      s += " (#{@year}): #{@title}"
      puts s
    end
  end
end

class Author
  attr_reader :name
  attr_reader :search_name

  def initialize(original_id, name, search_name)
    @original_id = original_id
    @name = name
    @search_name = search_name
  end
end

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: books.rb [options]"

  opts.on("-a", "--author AUTHOR", "Search by author") do |a|
    options[:author] = a
  end

  opts.on("-l", "--language LANG", "Search by langugae") do |l|
    options[:lang] = l
  end

  opts.on("-p", "--publishing PUBL", "Search by publishing") do |p|
    options[:publ] = p
  end
end.parse!

sep = { :books => '====== books ======',
        :languages => '====== languages ======',
        :authors => '====== authors ======',
        :books_authors => '====== books_authors ======',
        :publishers => '====== publishers ======' }

last_sep = nil
all_books = []
all_authors = []
all_languages = []
all_publishers = []
table_changed = false

CSV.foreach(BOOKS_FILE) do |row|
  if table_changed
    table_changed = false # to skip line defining rows after changing table
  else
    sep.each_pair do |key, value|
      if value == row[0]
        last_sep = key
        table_changed = true
      end
    end
    unless table_changed
      case last_sep
      when :books
        id = row[0]
        title = row[1]
        language_id = row[2]
        publisher_id = row[3]
        year = row[4]
        authors = []
        all_books[id.to_i - 1] = Book.new(title, language_id.to_i - 1, publisher_id.to_i - 1, year, authors)
      when :authors
        id = row[0]
        name = row[1]
        search_name = row[2]
        all_authors[id.to_i - 1] = Author.new(id.to_i - 1, name, search_name)
      when :books_authors
        author_id = row[1]
        book_id = row[2]
        all_books[book_id.to_i - 1].authors.push(all_authors[author_id.to_i - 1])
      when :languages
        all_languages[row[0].to_i - 1] = row[1]
      when :publishers
        all_publishers[row[0].to_i - 1] = row[1]
      end
    end
  end
end

options.each_pair do |key, value|
  books = []
  case key
  when :author
    all_books.each do |book|
      book.authors.each do |author|
        if author.name == value || author.search_name == value
          books.push book
        end
      end
    end
  when :lang
    all_books.each do |book|
      book_language = all_languages[book.language_id]
      if book_language == value
        books.push book
      end
    end
  when :publ
    all_books.each do |book|
      book_publisher = all_publishers[book.publisher_id]
      if book_publisher == value
        books.push book
      end
    end
  end
  all_books &= books
end

unless options.size == 0
  all_books.each(&:print)
end
